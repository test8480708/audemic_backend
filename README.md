# Project Name
Audemic Prompt Engine

## Description
The Audemic Prompt Engine project offers Database Seed functionality for Elasticsearch integration, Elasticsearch bulk indexing for prompt import, and a robust Search Controller for efficient querying. Local setup entails prerequisites like Ruby, Rails, and Docker, along with configuring environment variables. Users can effortlessly run and test the project locally, utilize Docker for a simplified setup, or access it directly via the deployed URL on Heroku.

**Audemic Prompt Engine Url-** https://audemic-app-new-48ec85db38a6.herokuapp.com

## Features
### Prompt Indexing and Seeding

- We have implemented prompt indexing and seeding to populate the data into the elastic search. We have used a third party to get some test data and indexed it into the elastic search using database seeding technique.
1. **File location:**
audemic_backend_app/prompt_engine/db/seeds.rb
2. **Run Seed Script:**
rails db:seed

### ElasticSearch
- We are using Elasticsearch to efficiently manage search functionality and provide quick results by looking into the various fields in the database.

### Views
- We have created a simple UI for search bar and a section that displays the search results. The search bar allows the user to type in the search term and behind the scenes, we are leveraging elasticsearch to get all the matching results quickly.
- We are considering that the search term is indexed in our elastic search database during the seeding process and hence we are able to get the results quickly.
* **Endpoint** - GET /prompt/search:

### Search Tracker
- We have added a search tracker that logs and monitors the search activity of the users and stores it in the database for further analysis.

* **Important Note:-** 
1. We import 100 records into Elasticsearch due to constraints in the Hugging Face API, which permits fetching only 100 records at a time. However, it's flexible to fetch a larger dataset if necessary.
2. Stop words filtering is not currently implemented in the provided script. To enhance the indexing process, consider adding functionality to filter stop words during prompt import. 

## Local Setup
### Prerequisites
Ruby (version 3.1.3)
Rails (version 7.1.3.2)
Elasticsearch (version 7.2)
Docker

### Create .env File inside 'prompt_engine' directory

### .env
- AUDEMIC_PROMPT_ENGINE_DATABASE_PASSWORD=audemic123
- SECRET_KEY_BASE=89a3e9dc9925a7e37e09b96aa44f0869
- POSTGRES_USER=vivek
- POSTGRES_PASSWORD=12345
- ELASTICSEARCH_URL=http://localhost:9200
- POSTGRES_HOST=localhost
- ELASTIC_SEARCH_PASSWORD=1PxMVQIqGI7k7+CHaJNC

### Create .env File
- Create a file named .env in the root of your project to store environment variables.

#### .env
- SECRET_KEY_BASE=89a3e9dc9925a7e37e09b96aa44f0869
- POSTGRES_USER=postgres
- POSTGRES_DB=postgres
- POSTGRES_PASSWORD=promptfinder
- POSTGRES_HOST=192.168.32.3
- ELASTICSEARCH_URL=http://elasticsearch:9200

### Steps to Run Locally

1. **Clone the repository:**
   ```bash
   - git clone https://gitlab.com/test8480708/audemic_backend.git
   - cd your-project
3. **Go to Root project directory:**
    - cd prompt_engine 
4. **Install dependencies:**
    - bundle install
5. **Set up the database:**
    - rails db:create
    - rails db:migrate
    - rails db:seed
6. **Start Postgresql server:**
    - service postgresql start
7. **Start ElasticSearch server:**
    - systemctl start elasticsearch.service
8. **Start the Rails server:**
    - rails server
9. Open your browser and visit http://localhost:3000 to access the application.

10. **To Run test cases:**
    - rspec

### Running with Docker

1. docker compose up --build (for elasticsearch + postgres)
2. docker compose -f docker-compose-prompt.yaml up --build (for elastic-prompt-search)

    **#NOTE:**

    1. On the local machine, Elasticsearch and PostgreSQL servers must be stopped.
    2. Run both above commands for docker in two different terminals


### How to Use

1. Access the search page for prompt- https://audemic-app-new-48ec85db38a6.herokuapp.com
1. Search data like:
    - "a simple"
    - "A portrait"
    - "the persistence"
    - "Realistic car"
    - "Island inside of"