module ElasticsearchClient
  class << self
    # Creates or retrieves a configured Elasticsearch client instance.
    # Calls the build_client method to generate configuration and establish the connection. 
    def client
      @client ||= build_client
    end

    private

    # Builds an Elasticsearch client instance with specified configuration settings.
    def build_client
      Elasticsearch::Client.new(configuration) 
    end

    # Returns a hash of configuration options for the Elasticsearch client.
    def configuration
      {
        url: elasticsearch_url,
        transport_options: transport_options,
        user: ENV['ELASTIC_SEARCH_USERNAME'],
        password: ENV['ELASTIC_SEARCH_PASSWORD'],
        logger: Rails.logger
      }
    end

    # Returns the Elasticsearch URL, either from the environment or a default.
    def elasticsearch_url
      ENV['ELASTICSEARCH_URL'] || 'https://localhost:9200'
    end

    # Returns transport options for the Elasticsearch client, including
    def transport_options
      {
        ssl: { verify: false },
        headers: { 'Content-Type' => 'application/json' },
        request: { timeout: 5 }
      }
    end
  end
end
