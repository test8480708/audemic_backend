module Utils
    module Retryable
      def with_retry(max_retries: 4, base_delay: 2)
        retries = 0
        delay = base_delay
  
        begin
          yield
        rescue Faraday::TimeoutError, Elasticsearch::Transport::Transport::Errors::ServiceUnavailable, Elasticsearch::Transport::Transport::Errors::Conflict => error
          if retries < max_retries
            retries += 1
            Rails.logger.warn("Elasticsearch service is unavailable. Retrying (attempt #{retries})...")
            sleep(delay)
            delay *= 2  # exponential backoff
            retry
          else
            Rails.logger.error("Failed after #{max_retries} retries.")
            raise error
          end
        end
      end
    end
  end
  