require 'will_paginate/array' 

class PromptsController < ApplicationController
  before_action :setter
  after_action :track_search_activity, only: [:search]

  attr_reader :prompt_service

  def setter(prompt_service = PromptService.new)
    @prompt_service = prompt_service
  end

  # GET /prompts
  def retrieve_all
    begin
      # Fetch the list of prompts using the provided page parameter.
      @prompts = prompt_service.list_prompts(params[:page])
      render "index", status: (@prompts.blank? ? :not_found : :ok)
    rescue PromptServiceException => error
      flash.now[:alert] = "We encountered a temporary issue while retrieving prompts. Please try again soon."
      render 'index', status: :internal_server_error
    end
  end

  # GET /prompts/search 
  def search
    begin
      unless params[:query].present?
        redirect_to root_path
      end
      @prompts = prompt_service.perform_search(params[:query], params[:page])
    rescue PromptServiceException => error
      flash.now[:alert] = "Internal error occured. Please try again."
    end
    # sending status 'ok' to control view and flashing errors, 
    # can be changed based on view choices
    render "index", status: :ok
  end

  private
  def track_search_activity
    PromptSearchTrackerJob.perform_later(params[:query])
  end
end
