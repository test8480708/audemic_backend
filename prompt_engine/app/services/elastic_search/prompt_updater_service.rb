module ElasticSearch
  class PromptUpdaterService
    attr_reader :prompt_service

    def initialize(uri, prompt_service = PromptService.new)
      @base_uri = uri
      @prompt_service = prompt_service
    end

    # Method to fetch prompts data from the external source
    def import_prompts
      prompts = []
      offset = 0
      batch_size = 100
      is_dataset_empty = false
      count = 0
      begin
        prompt_service.delete_prompts("prompts")
        
        Rails.logger.info("Fetching all prompts. It may take a while.")
        # Loop through each batch of records
        loop do
          uri = build_uri(offset)

          # Sending a GET request to the constructed URI to retrieve prompts data
          response = fetch_data_from_uri(uri)

          # Handle the response from the external source
          handle_response(uri, response) do |parsed_data|

          # Extract prompts from parsed data
          prompts += extract_prompts(parsed_data)

          # Increment offset for the next batch
          offset = next_batch(offset, batch_size)

          # Import data in elastic search provider
          prompt_service.import_prompts(prompts)
          prompts = []
          
          # Checking prompt emptyness
          is_dataset_empty = true if check_emptyness(parsed_data)
          end
          
          if is_dataset_empty || count == ENV["DEFAULT_DATA_SIZE"].to_i
            Rails.logger.info("Successfully imported #{count} prompts.") 
            break
          else
            count += batch_size
          end
        end
      rescue StandardError => error
        Rails.logger.error("#{$PROMPT_UPDATER_FAILED}: #{error}")
        raise PromptUpdaterServiceException.new("#{$PROMPT_UPDATER_FAILED}", error)
      end
    end

    private

    # Build the URI with the given offset
    def build_uri(offset)
      URI("#{@base_uri}&offset=#{offset}")
    end

    # Fetch data from the given URI
    def fetch_data_from_uri(uri)
      Net::HTTP.get_response(uri)
    end

    # Handle the response from the external source
    def handle_response(uri, response)
      # Check if response is successful
      if response.code == '200'
        yield JSON.parse(response.body) 
      else
        Rails.logger.error("HTTP request failed with status code: #{response.code}. URI: #{uri}")
      end
    end

    # Extract prompts from the parsed data
    def extract_prompts(data)
      data['rows'].map { |row| row['row']['Prompt'] }
    end

    # Calculate the offset for the next batch
    def next_batch(offset, batch_size)
      offset + batch_size 
    end

    # Check if the dataset is empty
    def check_emptyness(data)
      data['rows'].empty?   
    end
  end
end