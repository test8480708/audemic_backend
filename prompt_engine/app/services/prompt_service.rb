require_relative '../repositories/prompt_repository'

class PromptService
  include PromptHelper

  attr_reader :prompt_repository

  def initialize(prompt_repo = PromptRepository.new)
    @prompt_repository = prompt_repo
  end

  def list_prompts(page)
    begin
      prompt_results = prompt_repository.list
      return paginate(prompt_results, page)
    rescue PromptServiceException => error
      Rails.logger.error("#{$LIST_PROMPTS_FAILED}: #{error}")
      raise PromptServiceException.new("#{$LIST_PROMPTS_FAILED}", error)
    end
  end
  
  def perform_search(search_query, page)
    begin
      prompt_results = prompt_repository.find(search_query)
      return paginate(prompt_results, page)
    rescue StandardError => error
      Rails.logger.error("#{$PERFORM_SEARCH_FAILED}: #{error}")
      raise PromptServiceException.new("#{$PERFORM_SEARCH_FAILED}", error)
    end
  end
  
  def import_prompts(prompts)
    begin
      bulk_request_body = build_bulk_request_body(prompts)  
      prompt_repository.save(bulk_request_body)      
    rescue StandardError => error
      Rails.logger.error("#{$IMPORT_PROMPTS_FAILED}: #{error}")
      raise PromptServiceException.new("#{$IMPORT_PROMPTS_FAILED}", error)
    end
  end

  def delete_prompts(index_name)
    begin
      prompt_repository.delete(index_name)
    rescue StandardError => error
      Rails.logger.error("#{$DELETE_PROMPTS_FAILED}: #{error}")
      raise PromptServiceException.new("#{$DELETE_PROMPTS_FAILED}", error)
    end
  end
end
