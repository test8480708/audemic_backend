module PromptHelper
  def parse_data(search_results)
    hits = JSON.parse(search_results.to_json)['hits']['hits']
    data = hits.map { |hit| hit['_source']['data'] }
  end

  def paginate(data, page)
    data.paginate(page: page, per_page: $PAGINATION_SIZE )
  end

  def build_bulk_request_body(prompts)
    # Iterate over prompts
    bulk_request_body = prompts.map do |prompt|
      # Build index metadata
      index_metadata = { index: { _index: 'prompts' } }
      # Build bulk request body
      [index_metadata.to_json, { 'data': prompt , 'length': prompt.length}.to_json].join("\n")
    end

    return bulk_request_body
  end
end
