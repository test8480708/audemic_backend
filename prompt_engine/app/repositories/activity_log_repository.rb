class ActivityLogRepository
  def LogActivity(search_keyword)
    begin
      ActivityLog.create(search_keyword: search_keyword)
    rescue StandardError => error
      raise error
    end
  end
end
