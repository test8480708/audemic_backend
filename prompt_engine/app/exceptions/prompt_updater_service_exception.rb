class PromptUpdaterServiceException < StandardError
  def initialize(message = 'Internal error occurred', error)
    message = "#{message}: #{error.message}"
    super(message)
  end
end
