import { Controller } from "@hotwired/stimulus"

// Connects to Stimulus
export default class extends Controller {
  connect() {
    this.adjustFontSizeForResponsiveness(); // Initial resize
    window.addEventListener('resize', this.adjustFontSizeForResponsiveness);
  }

  adjustFontSizeForResponsiveness() {
    const container = this.element; // Assumes Stimulus target
    const originalFontSize = 20; // px 
   
    let currentFontSize = originalFontSize;
    while (container.scrollWidth > container.clientWidth) {
      currentFontSize--;
      container.style.fontSize = `${currentFontSize}px`;
    }
  }
};
